\documentclass[11pt]{beamer}

\usetheme[progressbar=frametitle,sectionpage=simple]{metropolis}
\usepackage{appendixnumberbeamer}
\usepackage{booktabs}
\usepackage{hyperref}
\usepackage[scale=2]{ccicons}
%\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{bibentry}
\nobibliography*
\setbeamerfont{bibliography entry author}{size=\tiny}
\setbeamerfont{bibliography entry title}{size=\tiny}
\setbeamerfont{bibliography entry location}{size=\tiny}
\setbeamerfont{bibliography entry note}{size=\tiny}

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}
\usepackage{caption}
\DeclareCaptionFont{6pt}{\fontsize{6pt}{5pt}\selectfont}
\captionsetup{font=6pt,labelfont=6pt}
\usepackage{xspace}
\makeatletter
\newcommand*{\rom}[1]{\expandafter\@slowromancap\romannumeral #1@}
\makeatother
\newcommand{\themename}{\textbf{\textsc{metropolis}}\xspace}
\setbeamercolor{progress bar}{fg=lime,bg=lightgray}
\setbeamercolor{background canvas}{bg=white}



\title{Bachelor Thesis Introduction}
\subtitle{Attribute Representations for Word Spotting in Arabic Handwriting}
% \date{\today}
\date{}
\author{Hatem Hamad}
\institute{TU Dortmund - Computer Science \\ Chair \rom{12} - Pattern Recognition Group}
\titlegraphic{\includegraphics[height=0.7cm]{images/tud_logo_rgb.jpg}}
\usepackage{graphbox}
\setbeamertemplate{footline}{%
		\hspace*{.12cm}%
		\vspace*{.20cm}%
        \includegraphics[height=0.55cm]{images/tud_logo_rgb.eps}%
		\hspace*{.15\linewidth}%
        \inserttitle - H.\ Hamad%
        \hfill%
        \usebeamercolor[fg]{page number in head/foot}%
        \usebeamerfont{page number in head/foot}%
        \insertframenumber\,/\,\inserttotalframenumber\kern1em%
 }
\setbeamersize{text margin left=.5cm,text margin right=.5cm}
\makeatletter
\setlength{\metropolis@frametitle@padding}{1.2ex}
\makeatother
\begin{document}

\maketitle

\begin{frame}{Agenda}
  \setbeamertemplate{section in toc}[ball unnumbered]
  \tableofcontents[hideallsubsections]
\end{frame}
%\addtolength{\leftskip}{-5em} \addtolength{\rightskip}{-2em}% Adjust margins

\section{Motivation}
\begin{frame}{Text recognition and OCR}
	\begin{itemize}
		\item<1-> \textbf{Goal:} Transcription of a given image portraying text
		\item<2-> \textbf{Excels in:}
		\begin{itemize}
			\item<3-> Printed Documents
			\item<4-> Simple Layouts and known Fonts
		\end{itemize}
	\end{itemize}
%\onslide<1->{
\begin{figure}
	\includegraphics[height=.4\textheight]{images/text-recog-mobile-app.png}
	\caption*{\href{https://github.com/LucemAnb/Android-Text-Scanner}{Github - Android-Text-Scanner}}
\end{figure}
%}
\end{frame}
\begin{frame}{Text recognition and OCR}
	\textbf{Constraints regarding Handwritten documents:} \footnote[1]{\tiny\bibentry{GIOTIS17}}\pause[1]
	\begin{itemize}
		%\item Difficulties in segmenting characters or words
		\item<2-> Handwriting Variations
		\item<3-> Degeneration of the original documents
	\end{itemize}
	\only<1-2>{
		\begin{figure}
			\includegraphics[height=.4\textheight]{images/Variations-in-handwriting-style-sample-taken-from-IRONOFF-database.png}
			\caption*{Sample taken from IRONOFF database}
		\end{figure}
	}
	\only<3-3>{
		\begin{figure}
			\includegraphics[height=.4\textheight]{images/old-document-degenerated-arabic.jpg}
			\caption*{Source:   \href{http://expositions.bnf.fr/livrarab/grands/036.htm}{http://expositions.bnf.fr}}
		\end{figure}
	}
\end{frame}
\begin{frame}{Word spotting}
	\textbf{Word spotting} is a form of \emph{Content-based Image Retrieval (CBIR)}\pause
	\begin{description}
		\item[Goal] Retrieving \emph{relevant} word images from a given document collection \pause
		\item[Method] Searching whether a document image contains a query word, by directly \alert{\emph{characterizing image features}} at character, word, line or even document level. \footnote[1]{\tiny\bibentry{GIOTIS17}}
	\end{description} 
\end{frame}
\section{PHOCNet}
\begin{frame}{PHOC}
	\begin{itemize}
		\item<1-> \emph{Embedded Attributes Framework} initially proposed by \textit{Almazán et al.}\footnote[1]{\tiny\bibentry{Almazan14}}
		\item<2-> Encode word strings and word images as common representation using attributes
		\item<3-> The basic units are an \emph{alphabet of characters or unigrams}
	\end{itemize}
	\onslide<3->
	\begin{figure}
		\includegraphics[width=.9\linewidth]{figures/PHOC-histogram-at-level-1.jpg}
	\end{figure}
\end{frame}
\begin{frame}
	\textit{Pyramidal Histogram	Of Characters} \textbf{(PHOC)}\\
	\textbf{PHOC} encodes the presence or absence of basic units in certain sections of a string in a pyramidal way\footnote[1]{\tiny\bibentry{SudholtF16}}.
	\begin{figure}
		\includegraphics[height=.4\textheight]{figures/PHOC.PNG}
	\end{figure}
\end{frame}
\begin{frame}{PHOCNet}
	Proposed and developed by \emph{Sebastian Sudholt}\footnote[1]{\tiny\bibentry{SudholtF16}}, \alert{PHOCNet} has outperformed state-of-the-art results for various word spotting benchmarks.\\ \onslide<1->
	\textbf{Characteristics}
	\begin{itemize}
		\item<2-> Deep Convolutional Neural Network
		\item<3-> Segmentation-based
		\item<4-> Outputs an Attribute vector in \textbf{PHOC} format
	\end{itemize}
\end{frame}
\begin{frame}{PHOCNet}
	\begin{figure}
		\includegraphics[height=.6\textheight]{figures/phocnet_architecture.png}
		\caption*{CNN architecture of PHOCNet \footnote[1]{\tiny\bibentry{SudholtF16}}}
	\end{figure}
\end{frame}
\section{Arabic script}
\begin{frame}{Arabic Language and script}
	\begin{itemize}
%		\item<+-> The largest of the Semitic languages
		\item<1-> The Arabic script is a writing system used for writing Arabic and several other languages, such as \alert{Persian}, \alert{Kurdish}, \alert{Azerbaijani}, \alert{Urdu} and \alert{Ottoman Turkish}. \footnote[1]{\tiny\bibentry{mirdehghan2010persian}}
		\item<2-> Written from \emph{right to left}
		\item<3-> Both printed and written Arabic are \emph{cursive}
%		\item Ranked as the $4^{th}$ most widely spoken language in the world \footnote{\href{https://www.ethnologue.com/
%			}{www.ethnologue.com}}
		
	\end{itemize}
	\only<1-1>{
		\begin{figure}
			\includegraphics[height=.45\textheight]{images/SemiArab_kleiner.jpg}
		\end{figure}
	}
	\onslide<3->{
	\begin{figure}
		\includegraphics[width=.8\linewidth]{figures/arabic-longest-word.jpg}
		\caption*{Translation: Then we gave it to you to drink}
	\end{figure}
	}
\end{frame}
\begin{frame}{Arabic script Distinctiveness}
	\textbf{Distinctiveness}\\ 
	\onslide<1->
	\begin{enumerate}
		\item<2-> Alphabet: 28 Characters, No distinct upper and lower case forms
		\item<3-> Letters can exhibit \alert{up to four distinct} forms corresponding to different position within words
	\end{enumerate}
	\only<2-2>{
		\begin{figure}
			\includegraphics[height=.38\textheight]{images/arabic-alphabet.jpg}
			\caption*{First 8 Letters in Arabic based on the alphabetic order}
		\end{figure}
	}
	\only<3-3>{
		\begin{figure}
			\includegraphics[height=.4\textheight]{figures/arabic-letter-daad.png}
		\end{figure}
	}
\end{frame}
\begin{frame}{Arabic script Distinctiveness}
	\textbf{Distinctiveness}\\ 
	\begin{enumerate}
		\setcounter{enumi}{2}
		\item<1-> Each letter can have one of \alert{8 different} Diacritics
		\item<2-> Some Combinations form a \textit{ligature} (special shapes)
	\end{enumerate}
\onslide<2-2>{
	\begin{figure}
		\includegraphics[height=.4\textheight]{figures/arabic-ligature.png}
		\caption*{Figure from \cite{Elarian15}}
	\end{figure}
}

\end{frame}

\begin{frame}{Arabic script and PHOC}
	What happens when we try to map \emph{Attribute Representation} into the alphabet?\\
	\onslide<1->
	\begin{itemize}
		\item<2-> All characters variation in different	\emph{parts of the word} (PAWs)
		\item<3-> Representation's character set becomes \alert{very large}
	\end{itemize}
\onslide<2->{
	\begin{figure}
		\includegraphics[height=.5\textheight]{images/arabic-shapes-for-letter-based-on-its-position.jpg}
	\end{figure}
}
\end{frame}

\section{Method}
\begin{frame}{Suggested Approaches}
	We experiment by reducing the alphabet to a smaller set of characters by two ways
	\begin{itemize}
		\item<1-> \textbf{PHOCNet approach} \footnote[1]{\tiny\bibentry{SudholtF16}}
		\begin{itemize}
			\item<2-> Some diacritic was removed : \emph{Shadda} diacritic.
			\item<3-> Special two-character shape ligature models were mapped to their models without the	shape contexts
		\end{itemize}
	\end{itemize}
\end{frame}
\begin{frame}{Suggested Approaches}
	\begin{itemize}
		\item<1-> \textbf{Sub-Character approach} \footnote[2]{\tiny\bibentry{Ahmad14:sub-char}}
		\begin{itemize}
			\item<2-> Sharing of common patterns between different forms of a character
			\item<3-> And between different characters
		\end{itemize}
	\end{itemize}
\only<1-1>{
	\begin{figure}
		\includegraphics[height=.5\textheight]{figures/SubCharacterExample-letters.png}
	\end{figure}
}
\only<2->{
	\begin{figure}
		\includegraphics[height=.5\textheight]{figures/SubCharacterExample.png}
	\end{figure}
}
\end{frame}

\section{Experiments}
\begin{frame}{The IFN/ENIT Dataset}
	\begin{itemize}
		\item Benchmark dataset for Arabic scripts
		\item More than	26,000 handwritten word images
		\item Written by 411 different writers
		\item Divided into 5 subsets from $a$ to $e$ 
	\end{itemize}
	\begin{figure}
		\includegraphics[height=.3\textheight]{figures/ifn-enit-samples_small1.png}
		\caption*{Image from the IFN/ENIT \href{http://www.ifnenit.com/samples.htm}{Website} representing the same town name written bei 12 different writers}
	\end{figure}
\end{frame}
\begin{frame}
	\begin{figure}
		\includegraphics[height=.85\textheight]{images/Inkedifn-enit-formular-sample_LI.jpg}
		\caption*{Image from the IFN/ENIT \href{http://www.ifnenit.com/samples.htm}{Website} representing a formular to filled by the writers}
	\end{figure}
\end{frame}

\begin{frame}{Evaluation Protocol}
	\begin{itemize}
		\item<1-> Performance Measures for an information retrieval problem
		\item<2-> A retrieval list consists of word images from the dataset
	\end{itemize}
	\onslide<2->{
		\begin{figure}
			\includegraphics[height=.5\textheight]{figures/word-spotting-list.PNG}
			\caption*{Image from Prof. Fink slides on Word spotting}
		\end{figure}
	}
\end{frame}
\begin{frame}{Evaluation Protocol}
	\begin{itemize}
		\item<1-> \emph{Average Precision} (AP)
		\item<2-> Use each unique word image as a query once
		\item<3-> \emph{mean Average Precision}(mAP)
		\item<4-> Evaluate both proposed approaches on \emph{PHOCNet}
		\item<5-> Compare to others in the Literature
	\end{itemize}
\end{frame}
\section{Summary}
\begin{frame}{Summary}
	\begin{itemize}
		\item<+-> \alert{Word spotting} is a \emph{recognition-free} approach best suited to index deformed document image collection
		\item<+-> \alert{Attributes Framework} enable Classification by Attributes/Features that may be shared across other classes
		\item<+-> \alert{\emph{PHOCNet}} achieved state-of-the-art results on Latin script
		\item<+-> \alert{Arabic script} has some peculiarities which result in a big set of characters for Attribute Representations
		\item<+-> \alert{Sub-Character} approach help reduce size of the set of characters
		\item<6-> Experiments will be carried out on \alert{IFN/ENIT} and evaluated using \alert{\emph{mAP}}
	\end{itemize}
\end{frame}
{\setbeamercolor{palette primary}{fg=black, bg=white}
\begin{frame}[standout]
  Thank You \\ for your attention.
\end{frame}
}

\appendix

%reset bibliography font sizes
\setbeamerfont{bibliography entry author}{size=\normalsize}
\setbeamerfont{bibliography entry title}{size=\normalsize}
\setbeamerfont{bibliography entry location}{size=\normalsize}
\setbeamerfont{bibliography entry note}{size=\small}
\begin{frame}[allowframebreaks]{References}
  
  \bibliography{hamad_ba_20}
  \bibliographystyle{apalike}
\end{frame}

\end{document}
